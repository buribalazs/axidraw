# Axidraw automaton Beta

### How to istall

1. Install [NODE VERSION MANAGER](https://github.com/creationix/nvm#installation) <- click link for how-to
1. Install NodeJS **USING** nvm. `nvm install --lts`, `nvm use --lts`
1. Make sure you have [GIT](https://git-scm.com/) installed
1. Git Clone the project repository with the command `git clone https://buribalazs@bitbucket.org/buribalazs/axidraw.git`
1. in the repository folder run the command `npm install`
1. create a google service account with permissions to read/write. [See this page](https://support.google.com/a/answer/7378726?hl=en)
1. Place the service account's authentication JSON file into the `cfg` folder inside the repository folder
1. rename the file to `google_auth.json`
1. Share the google sheet with the **service account's** email address (reaqd/write access)
1. in the repo folder run the command `npm run config`
1. answer the questions shown. You can run this again if something goes wrong.
1. run the command `npm start` to render svg files and add their download links to the sheet. If all ok, this will send an email to admin
1. run the command `npm run validate` to find invalid rows and notify reps about them
1. run the command `npm run sandbox` to start a server on port 80 where you can try the rendering. http://localhost:80

email buribalazs@gmail.com with questions/errors or message me on upwork

