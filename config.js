const { logger } = require('./src/logger.js');
const fs = require('fs');
const inquirer = require('inquirer');

let tpl;
try {
    tpl = JSON.parse(fs.readFileSync('./cfg/config.tpl.json'));
} catch (e) {
    logger.err(e).exit()
}


function q (text, obj, key) {
    text = {
        type: 'input',
        name: 'input',
        message: text,
        validate: function(value) {
            if (value) {
                return true;
            }

            return 'Please enter a value';
        }
    };
    return inquirer.prompt([text]).then(answers => {
        obj[key] = answers.input;
    });
}

async function main () {
    await q('email service for outgoing emails (gmail, smtp.yourdomain.com, etc)',
        tpl.mailserver, 'service');
    await q('outgoing email address (example@example.com)',
        tpl.mailserver.auth, 'user');
    await q('outgoing email password',
        tpl.mailserver.auth, 'pass');
    await q('admin email to send reports to (admin@example.com)',
        tpl, 'adminEmail');
    await q('google sheet id (sheet must be shared with your service account with read/write access)',
        tpl, 'googleSheetId');
    try {
        fs.writeFileSync('./cfg/config.json', JSON.stringify(tpl));
        logger.ok('CONFIG SAVED to ./cfg/config.json')
    } catch (e) {
        logger.err(e).exit();
    }
}

main().catch(e => logger.err(e).exit());
