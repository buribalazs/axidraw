const { CFG } = require('./src/cfg.js');
const { google } = require('googleapis');
const { rowParser } = require('./src/rowParser.js');
const { logger } = require('./src/logger.js');
const { jobProcessor } = require('./src/jobProcessor.js');
const { warningMails } = require('./src/warningMails.js');

async function main () {
    const auth = await google.auth.getClient({
        scopes: [
            'https://www.googleapis.com/auth/spreadsheets',
            'https://www.googleapis.com/auth/drive'
        ]
    });
    try {
        const project = await google.auth.getDefaultProjectId();
        console.log('GOOGLE PROJECT:', project);
    } catch (e) {
        logger.err(e).exit()
    }

    const sheets = google.sheets({ version: 'v4', auth });
    const drive = google.drive({ version: 'v3', auth });
    drive.files.list({
        pageSize: 1,
        fields: 'nextPageToken, files(id, name)',
    }, (err) => {
        if (err) return logger.err('The Drive API returned an error: ' + err).exit();
    });
    sheets.spreadsheets.values.get({
        spreadsheetId: CFG.googleSheetId,
        range: 'A2:V',
    }, (err, response) => {
        if (err) return logger.err('The Sheets API returned an error: ' + err).exit();
        let rows = response.data.values;
        if (rows.length) {
            rows = rowParser.parse(rows);
            if (process.env.JOB === 'make') {
                let rowsToProcess = rows.filter(r => rowParser.validate(r) && !r.file);
                jobProcessor(drive, sheets, rowsToProcess)
                    .then(res => logger.ok('DONE.'))
                    .catch(e => logger.err(e))
            } else if (process.env.JOB === 'validate') {
                let invalidRows = rows.filter(r => !rowParser.validate(r)).filter(r => !!r.repEmail && !r.file);
                if (invalidRows.length){
                    warningMails(invalidRows)
                        .then(e => logger.ok('Validation errors sent to reps'))
                        .catch(e => logger.err(e));
                }else{
                    logger.ok('NO INVALID ROWS! :D');
                }
            }
        } else {
            logger.err('No data found in sheet.');
        }
    });
}

main().catch(console.error);
