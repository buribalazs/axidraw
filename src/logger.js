const chalk = require('chalk');

function err (e) {
    console.log(chalk.redBright('ERROR: ', Array.from(arguments).join(' ')));
    return {
        exit: (code) => process.exit(code || 0)
    };
}

function ok (e) {
    console.log(chalk.greenBright('SUCCESS: ', Array.from(arguments).join(' ')));
}

function info (e) {
    console.log(chalk.cyan('INFO: ', Array.from(arguments).join(' ')));
}

module.exports = {
    logger: {
        err,
        ok,
        info
    }
};
