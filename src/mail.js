const nodemailer = require('nodemailer');
const { CFG } = require('./cfg.js');
const { logger } = require('./logger.js');

function sendMail (to, subject, text) {
    return new Promise((resolve, reject) => {
        const transporter = nodemailer.createTransport(CFG.mailserver);
        const mailOptions = {
            from: CFG.mailserver.auth.user,
            to,
            subject,
            text
        };

        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                logger.err(error);
                reject();
            } else {
                logger.ok('Email sent: ' + info.response);
                resolve();
            }
        });
    });
}


module.exports = {
    sendMail
};
