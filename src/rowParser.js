const rowParser = {
    parse: rows => {
        return rows.map((r, i) => ({
            idx: i,
            dateAdded: r[0],
            repEmail: r[1],
            repName: r[2],
            repAddress: r[3],
            repCity: r[4],
            repState: r[5],
            repZip: r[6],
            repCountry: r[7],
            company: r[8],
            contact: r[9],
            address1: r[10],
            address2: r[11],
            city: r[12],
            state: r[13],
            zip: r[14],
            country: r[15],
            salutation: r[16],
            message: r[17],
            signature: r[18],
            font: r[19],
            file: r[20],
            dateMailed: r[21]
        })).map(r => {
            r.formattedSalutation = r.salutation + ',';
            r.formattedSignature = '-' + r.signature;
            r.senderAddress = r.repName + '\n' + r.repAddress + '\n' + r.repCity + ', ' + r.repState + ', ' + r.repZip + (r.repCountry ? '\n' + r.repCountry.toUpperCase() : '');
            r.recipientAddress = r.company + '\nATTN: ' + r.contact + '\n' + r.address1 + '\n' + (r.address2 ? r.address2 + '\n' : '') + r.city + ', ' + r.state + ' ' + r.zip + (r.country ? '\n' + r.country.toUpperCase() : '');
            return r;
        });
    },
    validate (row) {
        const requiredFields = [
            'repEmail',
            'repName',
            'repAddress',
            'repCity',
            'repState',
            'repZip',
            'company',
            'contact',
            'address1',
            'city',
            'state',
            'zip',
            'salutation',
            'message',
            'signature'
        ];
        row.errors = '';
        requiredFields.forEach(f => {
            if (!row[f]) {
                row.errors += `${f} required\n`;
            } else if (f === 'message' && row.message.length > 200) {
                row.errors.concat(`message longer than 200 characters\n`);
            }
        });
        return !row.errors;
    }
};

module.exports = { rowParser };
