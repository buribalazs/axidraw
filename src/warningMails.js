const { sendMail } = require('./mail.js');
const { logger } = require('./logger.js');

async function warningMails (rows) {
    let emails = {};
    rows.forEach(r => {
        let key = r.repEmail;
        if (!emails[key]) {
            emails[key] = 'Hi,\n\nThe following rows associated with your email address in the Axidraw google sheet have errors:\n\n';
        }
        emails[key] += `row: ${r.idx + 2}  errors:\n${r.errors}\n`;
    });
    return Promise.all(Object.keys(emails).map(email => {
        let message = emails[email];
        logger.info(`emailing ${email} about errors`);
        return sendMail(email, `Axidraw sheet errors ${(new Date()).toString()}`, message).catch(e => logger.err(`sending to email "${email}" failed`));
    }))
}

module.exports = {
    warningMails
};
