const express = require('express');
const path = require('path');
const STATIC_PATH = path.resolve('./src/sandbox/static');
const app = express();
const {render} = require('../svgRenderer.js');
app.use(express.static(STATIC_PATH));

const bodyParser = require('body-parser');
app.use(bodyParser.json());

app.post('/render', (req, res) => {
    const body = req.body;
    const mappedBody = {
        formattedSalutation: body.salutation,
        message: body.message,
        formattedSignature: body.signature,
        senderAddress: body['sender-address'],
        recipientAddress: body['recipient-address'],
        font: body.font
    };
    res.end(render(mappedBody));
});

const PORT = process.env.PORT || 80;
app.listen(PORT, () => console.log(`Sandbox listening on port ${PORT}!`));

