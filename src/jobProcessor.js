const { sendMail } = require('./mail.js');
const { render } = require('./svgRenderer.js');
const { logger } = require('./logger.js');
const { CFG } = require('./cfg.js');

async function jobProcessor (drive, sheets, rowsToProcess) {
    if (!rowsToProcess.length) {
        logger.info('no new jobs found...');
        return;
    }
    let links = await new Promise((resolve, reject) => {
        let i = 0;
        let r;
        let list = [];

        function cycle () {
            r = rowsToProcess[i];
            if (!r) {
                resolve(list);
            }
            iter(r)
                .then(res => {
                    list.push(res);
                    i++;
                    cycle();
                })
                .catch(e => reject(e));
        }

        cycle();

        function iter (r) {
            return new Promise((resolve, reject) => {
                let fileName = `axidraw-job-${(new Date()).toString().split(' ').join('-')}-${r.company.split(' ').join('-')}.svg`;
                logger.info(`uploading ${fileName}`);
                let svg = render(r);
                let fileMetadata = {
                    title: fileName,
                    name: fileName
                };
                let media = {
                    name: fileName,
                    mimeType: 'image/svg+xml',
                    body: svg
                };
                drive.files.create({
                    resource: fileMetadata,
                    media: media,
                    fields: 'id, webContentLink'
                }, function (err, file) {
                    if (err) {
                        logger.err(err);
                        reject(err);
                    } else {
                        logger.info(`setting read permission for ${fileName}`);
                        let body = {
                            'value': 'default',
                            'type': 'anyone',
                            'role': 'reader'
                        };

                        drive.permissions.create({
                            fileId: file.data.id,
                            resource: body,
                        }, (err, res) => {
                            if (err) {
                                logger.err(err);
                                reject(err)
                            } else {
                                resolve({ idx: r.idx, link: file.data.webContentLink });
                            }
                        });
                    }
                });
            });
        }
    });

    if (!links) return;
    let linkCol = new Array(Math.max(...links.map(l => Number(l.idx))) + 1).fill(null);
    links.forEach(l => {
        linkCol[l.idx] = l.link;
    });

    let request = {
        spreadsheetId: CFG.googleSheetId,
        resource: {
            valueInputOption: 'RAW',
            data: [
                {
                    range: 'U2',
                    majorDimension: 'COLUMNS',
                    values: [linkCol]
                }
            ]
        },
    };
    let sheetUpdate = await new Promise((resolve, reject) => {
        logger.info(`updating spreadsheet with new download links`);
        sheets.spreadsheets.values.batchUpdate(request, function (err, response) {
            if (err) {
                logger.err(err);
                reject(err);
            } else {
                logger.ok(response.data.totalUpdatedCells + ' cells updated in sheet');
                resolve(response.data.totalUpdatedCells);
            }
        });
    });

    if (!sheetUpdate) return;

    let messageBody = 'Hi,\n\nnew axidraw jobs are available for printing\n\n' + links.map(l => `row: ${l.idx + 2} file: ${l.link}`).join('\n');
    logger.info(`sending new links to admin email ${CFG.adminEmail}`);
    await sendMail(CFG.adminEmail, `new axidraw print jobs ${(new Date()).toString()}`, messageBody);
}

module.exports = {
    jobProcessor
};
