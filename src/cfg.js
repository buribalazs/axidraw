const fs = require('fs');
const path = require('path');
const { logger } = require('./logger.js');
let CFG;
try {
    CFG = JSON.parse(fs.readFileSync(path.resolve(__dirname + '/../cfg/config.json')).toString());
} catch (e) {
    logger.err(e, 'please run "npm run  config"').exit();
}

module.exports = {
    CFG
};