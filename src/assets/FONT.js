const { FONTS } = require('./FONTS.js');
const ALPHABET = ' !"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~°';

const FONT_NAMES = Object.keys(FONTS);

const FONT = FONT_NAMES.reduce((acc, val, i) => {
    acc[val] = FONTS[val].reduce((acc, val, i) => {
        let key = ALPHABET[i];
        let data = val.split(' ');
        acc[key] = {
            A: Number(data[0]),
            B: Number(data[1]),
            char: data.indexOf('M') > 0 ? data.slice(data.indexOf('M')).join(' ') : null,
            left: data.indexOf('M') > 0 ? calcLeft(data.slice(data.indexOf('M'))) : 0
        };
        return acc;
    }, {});
    return acc;
}, {});

function calcLeft (data) {
    if (data) {
        data = data.map(d => Number(d)).filter(d => !Number.isNaN(d)).filter((d, i) => i % 2 === 0);
        data = Math.min(...data);
        if (data < 0) {
            return -data * 2;
        }
    }
    return 0;
}

module.exports = { FONT };
