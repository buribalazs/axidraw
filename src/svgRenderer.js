const fs = require('fs');
const cheerio = require('cheerio');
const { FONT } = require('./assets/FONT.js');
const html = fs.readFileSync('./src/assets/GUIDES_CLEAN.svg');
const $ = cheerio.load(html);

const STYLE = 'fill:none;stroke:#000000;';
const LINE_HEIGHT = 32;
const ALIGN = {
    LEFT: 'left',
    CENTER: 'center',
    RIGHT: 'right'
};
Object.freeze(ALIGN);

const fields = [
    {
        id: 'message',
        align: ALIGN.LEFT,
        box: { w: 360, h: 80 },
        lh: 40,
        scale: 0.87,
        center: true,
        val: 'Thanks for taking the time with me this week at the factory. I appreciate all your help and hard work!'
    },
    { id: 'formattedSalutation', align: ALIGN.LEFT, box: { w: 380, h: 30 }, val: 'Jim,' },
    { id: 'formattedSignature', align: ALIGN.RIGHT, box: { w: 200, h: 30 }, val: '-Eric' },
    {
        id: 'senderAddress',
        align: ALIGN.LEFT,
        box: { w: 410, h: 180 },
        scale: 1,
        val: 'Greenberg\n6314 E Corrine Dr\nScottsdale, AZ, 85254'
    },
    {
        id: 'recipientAddress',
        align: ALIGN.CENTER,
        box: { w: 310, h: 148 },
        scale: 1,
        val: 'Howard Miller\nATTN: Jim O\'Keefe\n860 E Main Ave\nZeeland, MI 49464'
    }
];

let minScale = 1;

function drawChar (char, offset = 0, el, font) {
    offset = Number(offset);
    char = font[char];
    if (!char) return offset;
    offset -= char.A;
    if (!char.char) return offset;
    let path = $('<path></path>')
        .attr('d', char.char)
        .attr('transform', `translate(${offset},0)`)
        .attr('style', STYLE);

    el.append(path);
    offset += char.B;
    return offset;
}

function drawLine (str, font) {
    let offset = 0;
    let g = makeG();
    for (let i = 0; i < str.length; i++) {
        offset = drawChar(str[i], offset, g, font);
    }
    return { g, offset, left: (FONT[str.charAt(0)] || { left: 0 }).left };
}

function drawLines (str, align = ALIGN.LEFT, box, lineHeight, initialScale, center, font) {
    lineHeight = lineHeight || LINE_HEIGHT;
    let formatted = format(str, box, lineHeight, initialScale, font);
    str = formatted.str;
    let scale = formatted.scale;
    str = str.split('\n');
    let g = makeG();
    let vOffset = 0;
    for (let i = 0; i < str.length; i++) {
        let { g: line, offset: hOffset, left } = drawLine(str[i], font);
        line.attr('transform', `translate(${calcOffset(align, hOffset, left)}, ${vOffset}) scale(${scale})`);
        g.append(line);
        vOffset += lineHeight * scale;
    }
    if (center) {
        g.attr('transform', `translate(${(box.w - formatted.w) / 2},${(box.h - formatted.h) / 2})`);
    }

    return g;
}

function calcOffset (align, hOffset, left) {
    switch (align) {
        case ALIGN.RIGHT:
            return -hOffset;
        case ALIGN.CENTER:
            return -hOffset / 2;
        default:
            return left;
    }
}

function format (str, box = { w: 300, h: 300 }, lineHeight, scale, font) {
    scale = scale || minScale || 1;
    lineHeight = lineHeight || LINE_HEIGHT;
    str = str.split('\n').join('#nl# ');
    let words = str.split(' ').map(word => {
        let newline = false;
        if (word.includes('#nl#')) {
            newline = true;
            word = word.replace('#nl#', '');
        }
        return {
            word,
            width: measure(word, font),
            newline
        };
    });

    let lineCount = 1;
    let width = 0;
    let maxWidth = 0;
    do {
        lineCount = 1;
        width = 0;
        maxWidth = 0;
        words.forEach((w, i) => {
            w.break = false;
            if (w.newline || (width + (w.width + (words[i + 1] || { width: 0 }).width) * scale > box.w)) {
                w.break = true;
                maxWidth = Math.max(maxWidth, width + w.width * scale);
                width = 0;
                lineCount++;
            } else {
                width += w.width * scale;
            }
            maxWidth = Math.max(maxWidth, width);
        });
    } while (lineCount * lineHeight * scale > box.h && ((scale -= 0.01) || 1));

    str = words.map(w => w.word + (w.break ? '\n' : ' ')).join('');
    minScale = Math.min(minScale, scale * 1.2);
    return {
        str,
        scale,
        w: maxWidth,
        h: lineCount * lineHeight * scale
    }
}

function measure (str, font) {
    str += ' ';
    return Array.from(str).reduce((acc, val) => {
        let char = font[val];
        if (char) {
            return acc + char.B;
        }
        return acc;
    }, 0);
}

function makeG () {
    return $('<g></g>');
}

function render (row) {
    const fontName = row.font || 'EMSQwandry';
    minScale = 1;
    fields.forEach(f => {
        let g = $(`#${f.id}`);
        let result = drawLines(row[f.id], f.align || ALIGN.LEFT, f.box, f.lh, f.scale, f.center, FONT[fontName] || FONT.EMSQwandry);
        g.empty().append(result);
    });
    $('#mock-guides').remove();
    return $('body').html();
}

module.exports = {
    render
};
